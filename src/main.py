from wsgiref import headers
from utils import database as sql
from utils import user as my_user
import sys
import tabulate 
import mysql.connector
import traceback
import getpass


def main():    

    host = input("Host IP: ")
    my_db = input("Database name: ")
    name = input("Username: ")
    password = getpass.getpass("Password: ")

    try:
        with my_user.User(name, password, host) as guest:
            connection = sql.Database(guest, my_db)

            print("\nSelect '1' to write a query\n")
            print("\nSelect '2' to close the current connection\n")

            while(True):
                choice = input(">> ")   
                if choice == '1':
                    sql_statement = input("\nWrite your SQL query >>  ")
                    print(
                        sql.Database.display_data(guest.create_query(connection,
                        sql_statement), connection._cursor.column_names))
                if choice == '2':
                    connection.close()
                    sys.exit()
    except:
        traceback.print_exc()      
    finally:
        connection.close()
        sys.exit()


if __name__ == '__main__':
    main()
