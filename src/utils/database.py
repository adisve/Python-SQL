#!/usr/bin/python
import tabulate 
import mysql.connector as sql
class Database:
    def __init__(self, user, db):
        self._conn = sql.connect(user=user.name, password=user.password,
                database=db, host=user.host)
        self._cursor = self._conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    @property
    def connection(self):
        return self._conn

    @property
    def cursor(self):
        return self._cursor

    def commit(self):
        self.connection.commit()

    def close(self, commit=True):
        if commit:
            self.commit()
        self.connection.close()

    def execute(self, sql, params=None):
        self.cursor.execute(sql, params or ())

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def query(self, sql, params=None):
        self.cursor.execute(sql, params or ())
        return self.fetchall()

    def display_data(data, column_names):
        return(tabulate.tabulate(data, column_names,
            tablefmt='psql'))
        
