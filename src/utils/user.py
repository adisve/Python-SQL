import traceback
class User:
    def __init__(self, username, password, host):
        self.name = username
        self.password = password
        self.host = host

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            traceback.print_exception(exc_type, exc_value, tb)
            return False 
        return True

    def create_query(self, connection, sql):
        try:
            return connection.query(sql)
        except:
            traceback.print_exc()
            return

    
